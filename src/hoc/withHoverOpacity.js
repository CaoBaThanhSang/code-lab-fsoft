import React from 'react';
import {View} from 'react-native';

export const WithHoverOpacity = ChildComponent => {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        opacity: 1,
      };
    }
    onMouseEnter() {
      this.setState({
        opacity: 0.5,
      });
    }
    onMouseLeave() {
      this.setState({
        opacity: 1,
      });
    }
    render() {
      return (
        <View
          style={{opacity: this.state.opacity}}
          onTouchStart={() => this.onMouseEnter()}
          onTouchEnd={() => this.onMouseLeave()}>
          <ChildComponent />
        </View>
      );
    }
  };
};
