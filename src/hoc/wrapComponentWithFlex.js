import React from 'react';
import {View} from 'react-native';

export const WrapComponent = ChildComponent => {
  class WrapComponentHOC extends React.Component {
    render() {
      return (
        <View style={{flex: 1}}>
          <ChildComponent />
        </View>
      );
    }
  }
  return WrapComponentHOC;
};
