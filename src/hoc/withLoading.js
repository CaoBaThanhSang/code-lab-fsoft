import React from 'react';
import {Text, ActivityIndicator, View} from 'react-native';

export const WithLoading = ChildComponent => {
  const WithLoadingComponentHOC = ({isLoading, ...props}) => {
    if (!isLoading) return <ChildComponent {...props} />;
    return (
      <View>
        <ActivityIndicator animating={isLoading} size={24} color={'red'} />
      </View>
    );
  };
  return WithLoadingComponentHOC;
};
