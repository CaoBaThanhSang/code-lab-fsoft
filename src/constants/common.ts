import {ImageSourcePropType} from 'react-native';
import {Images} from '../images';

interface Recipe {
  id: string;
  title: string;
  image: ImageSourcePropType;
}

export const recipesData: Array<Recipe> = [
  {
    id: 'r1',
    title: 'Sweets',
    image: Images.img_sweets,
  },
  {
    id: 'r2',
    title: 'Italian',
    image: Images.img_italian,
  },
  {
    id: 'r3',
    title: 'Desserts',
    image: Images.img_desserts,
  },
  {
    id: 'r4',
    title: 'Chocolates',
    image: Images.img_chocolates,
  },
  {
    id: 'r5',
    title: 'Egg',
    image: Images.img_egg,
  },
  {
    id: 'r6',
    title: 'Noodle',
    image: Images.img_noodle,
  },
];

export const recipesSavedData: Array<Recipe> = [
  {
    id: 'r1',
    title: 'Sweets',
    image: Images.img_desserts,
  },
  {
    id: 'r2',
    title: 'Italian',
    image: Images.img_desserts,
  },
  {
    id: 'r3',
    title: 'Desserts',
    image: Images.img_desserts,
  },
  {
    id: 'r4',
    title: 'Chocolates',
    image: Images.img_desserts,
  },
  {
    id: 'r5',
    title: 'Egg',
    image: Images.img_desserts,
  },
  {
    id: 'r6',
    title: 'Noodle',
    image: Images.img_desserts,
  },
];

export const recipesFollowingData: Array<Recipe> = [
  {
    id: 'r1',
    title: 'Sweets',
    image: Images.img_chocolates,
  },
  {
    id: 'r2',
    title: 'Italian',
    image: Images.img_chocolates,
  },
  {
    id: 'r3',
    title: 'Desserts',
    image: Images.img_chocolates,
  },
  {
    id: 'r4',
    title: 'Chocolates',
    image: Images.img_chocolates,
  },
  {
    id: 'r5',
    title: 'Egg',
    image: Images.img_chocolates,
  },
  {
    id: 'r6',
    title: 'Noodle',
    image: Images.img_chocolates,
  },
];

export const trendingRecipesData: Array<Recipe> = [
  {
    id: 'tr1',
    title: 'Banana and Mandarin Buns',
    image: Images.img_banana,
  },
  {
    id: 'tr2',
    title: 'Coconut Pound Cake',
    image: Images.img_coconut,
  },
  {
    id: 'tr3',
    title: 'Cardamom and Cranberry Pastry',
    image: Images.img_cardamom,
  },
];

export const recipesCookPotatoData: Array<Recipe> = [
  {
    id: 'rc1',
    title: 'Tenderized Salty & Sour Potato Beef',
    image: Images.img_tenderized,
  },
  {
    id: 'rc2',
    title: 'Sautéed Orange & Mustard Bruschetta',
    image: Images.img_sauteed,
  },
  {
    id: 'rc3',
    title: 'Blanched Peppermint Pheasant',
    image: Images.img_blanched,
  },
];

export const recipesCookBananaData: Array<Recipe> = [
  {
    id: 'rc1',
    title: 'Blanched Peppermint Pheasant',
    image: Images.img_blanched,
  },
  {
    id: 'rc2',
    title: 'Sautéed Orange & Mustard Bruschetta',
    image: Images.img_sauteed,
  },
  {
    id: 'rc3',
    title: 'Tenderized Salty & Sour Potato Beef',
    image: Images.img_tenderized,
  },
];

export const recipesTomatoData: Array<Recipe> = [
  {
    id: 'rc1',
    title: 'Sautéed Orange & Mustard Bruschetta',
    image: Images.img_sauteed,
  },
  {
    id: 'rc2',
    title: 'Tenderized Salty & Sour Potato Beef',
    image: Images.img_tenderized,
  },
  {
    id: 'rc3',
    title: 'Blanched Peppermint Pheasant',
    image: Images.img_blanched,
  },
];

export const recipesPumpkinData: Array<Recipe> = [
  {
    id: 'rc1',
    title: 'Sautéed Orange & Mustard Bruschetta',
    image: Images.img_sauteed,
  },
  {
    id: 'rc2',
    title: 'Blanched Peppermint Pheasant',
    image: Images.img_blanched,
  },
  {
    id: 'rc3',
    title: 'Tenderized Salty & Sour Potato Beef',
    image: Images.img_tenderized,
  },
];

interface RecipeData {
  id: string;
  title: string;
  description: string;
  likedAmount: number;
  commentedAmount: number;
  image: ImageSourcePropType;
  avatar: ImageSourcePropType;
  isLiked: boolean;
  datedPost: number;
  profileName: string;
}

export const recipeData: Array<RecipeData> = [
  {
    id: 'r1',
    title: 'Red Wine and Min Soufllé',
    description:
      'Apparently we had reached a great height in the atmosphere, for the shy was ...',
    likedAmount: 32,
    commentedAmount: 8,
    image: Images.img_red_wine,
    avatar: Images.img_avatar,
    isLiked: false,
    datedPost: 2,
    profileName: 'Profile Name',
  },
  {
    id: 'r2',
    title: 'White Wine Toffee',
    description:
      'Apparently we had reached a great height in the atmosphere, for the shy was ...',
    likedAmount: 32,
    commentedAmount: 8,
    image: Images.img_white_wine,
    avatar: Images.img_avatar,
    isLiked: false,
    datedPost: 2,
    profileName: 'Profile Name',
  },
  {
    id: 'r3',
    title: 'Vanilla Pud',
    description:
      'Apparently we had reached a great height in the atmosphere, for the shy was ...',
    likedAmount: 32,
    commentedAmount: 8,
    image: Images.img_vanilla_pud,
    avatar: Images.img_avatar,
    isLiked: false,
    datedPost: 2,
    profileName: 'Profile Name',
  },
  {
    id: 'r4',
    title: 'Cured Vegetables & Mutton',
    description:
      'Apparently we had reached a great height in the atmosphere, for the shy was ...',
    likedAmount: 32,
    commentedAmount: 8,
    image: Images.img_cured_vegetables,
    avatar: Images.img_avatar,
    isLiked: false,
    datedPost: 2,
    profileName: 'Profile Name',
  },
];

export interface UserProfile {
  userName: string;
  password: string;
  fullName?: string;
  bio?: string;
  email?: string;
  phone?: string;
  avatar?: string;
}

export const userProfileData: UserProfile = {
  userName: 'Sang Cao',
  password: '123',
  fullName: 'Sang Cao',
  bio: 'Potato Master',
  email: 'caobathanhsang@gmail.com',
  phone: '0584246112',
  avatar:
    'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/hinh-co-gai-xinh-dep-de-thuong_025104709.jpg',
};

export const Recipes = {
  id: 'r1',
  name: 'Sautéed Orange & Mustard',
  gallerys: [
    {
      id: 'g1',
      image: Images.img_cover_gallery1,
      isCover: true,
    },
    {
      id: 'g2',
      image: Images.img_cover_gallery2,
      isCover: false,
    },
    {
      id: 'g3',
      image: Images.img_cover_gallery1,
      isCover: false,
    },
    {
      id: 'g4',
      image: Images.img_cover_gallery2,
      isCover: false,
    },
  ],
  ingredients: [
    {
      id: 'i1',
      image: Images.img_avatar_big,
    },
    {
      id: 'i2',
      image: Images.img_avatar_big,
    },
  ],
  howToCook: [
    'Heat a Belgian waffle iron.',
    'Mix the flour, sugar, and baking powder together in a mixing bowl. Stir in 1 cup eggnog, butter, and the egg until well blended. Add more eggnog if needed to make a pourable batter.',
    'Lightly grease or spray the waffle iron with non-stick cooking spray. Pour some batter onto the preheat...',
  ],
  additionalInfo: {
    servingTime: '12 Mins',
    nutritionFacts:
      '222 calories 6.2 g fat 7.2 g carbohydrates 28.6 g protein 68 mg cholesterol 268 mg sodium',
    tags: 'Sweet, Coconut, Quick, Easy, Homemade',
  },
};

interface gallery {
  id: string;
  image: ImageSourcePropType;
  isCover: boolean;
}

interface ingredient {
  id: string;
  image: ImageSourcePropType;
}

interface additionalInfo {
  servingTime: string;
  nutritionFacts: string;
  tag: string;
}

interface _Recipes {
  id: string;
  name: string;
  gallerys: Array<gallery>;
  ingredients: Array<ingredient>;
  howToCook: string[];
  additionalInfo: additionalInfo;
}
