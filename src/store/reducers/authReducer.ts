import {authTypes} from '../actions/actionTypes';

const initialState = {
  users: [
    {
      id: '1',
      userName: 'Sang Cao',
      password: '123',
      fullName: 'Sang Cao',
      bio: 'Potato Master',
      email: 'caobathanhsang@gmail.com',
      phone: '0584246112',
      avatar:
        'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/hinh-co-gai-xinh-dep-de-thuong_025104709.jpg',
    },
    {
      id: '2',
      userName: 'Le Trieu',
      password: '456',
      fullName: 'Le Trieu',
      bio: 'Potato Master',
      email: 'lehuynhhaitrieu@gmail.com',
      phone: '0584246112',
      avatar:
        'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/hinh-co-gai-xinh-dep-de-thuong_025104709.jpg',
    },
  ],
  user: {},
  loginPending: false,
};

interface Action {
  type: string;
}

const authReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case authTypes.LOGIN_PENDING:
      return {...state, loginPending: true};
    case authTypes.LOGIN_SUCCESS:
      const user = action.data;
      return {...state, user};
    case authTypes.LOGOUT_SUCCESS:
      const userLogout = action.data;
      return {...state, user: userLogout};
  }
  return state;
};

export default authReducer;
