import {authTypes} from './actionTypes';
import {UserProfile} from '../../constants';

export const authActions = {
  login,
  logout,
  loginPending,
};

function login(data: UserProfile, resolve: () => void) {
  return {
    type: authTypes.LOGIN,
    data,
    resolve,
  };
}

function logout(resolve: () => void) {
  return {
    type: authTypes.LOGOUT,
    resolve,
  };
}

function loginPending() {
  return {
    type: authTypes.LOGIN_PENDING,
  };
}
