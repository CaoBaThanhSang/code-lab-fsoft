import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Images} from '../../images';
import {Scale} from '../../utils';

interface Props {
  title?: string,
  onBackClick?: () => void,
  isNotice?: boolean
}

export const Header: React.FC<Props> = ({title, onBackClick, isNotice}) => {
  return (
    <View style={styles.header}>
      {onBackClick ? (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={onBackClick}>
            <Image source={Images.img_back} />
          </TouchableOpacity>
          <Text style={{color: '#767676', marginLeft: 12}}>
            Back to Profile
          </Text>
        </View>
      ) : (
        <View style={{flexDirection: 'row'}}>
          <Image source={Images.img_logo} />
          <Text style={styles.title}>{title}</Text>
        </View>
      )}

      {isNotice && (
        <View style={{flexDirection: 'row'}}>
          <Image
            source={Images.img_notification}
            style={styles.notificationImage}
          />
          <Image source={Images.img_message} style={{width: 24, height: 24}} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    height: Scale.scale(70),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: Scale.scale(10),
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
  },
  notificationImage: {
    width: 24,
    height: 24,
    marginRight: 30,
  },
});
