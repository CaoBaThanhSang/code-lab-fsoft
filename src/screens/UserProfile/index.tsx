import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from 'react-native';
import {Images} from '../../images';
import {
  recipesData,
  recipesFollowingData,
  userProfileData,
} from '../../constants';
import {RecipeItem} from './components';
import {Colors} from '../../themes';
import {SCREEN} from '../../routes/Screen';
import {RecipesStorage, UserProfileStorage} from '../../utils';
import {Scale} from '../../utils';
import {WithLoading} from '../../hoc/withLoading';
import {WithHoverOpacity} from '../../hoc/withHoverOpacity';
import {useSelector, RootStateOrAny, useDispatch} from 'react-redux';
import {authActions} from '../../store/actions';
import {NavigationScreenProp} from 'react-navigation';

type UserProfileNavigationProps = NavigationScreenProp<{}>;
interface UserProfileProps {
  navigation: UserProfileNavigationProps;
}

export const UserProfileScreen: React.FC<UserProfileProps> = props => {
  interface menu {
    id: number;
    name: string;
    amount: number;
  }
  const menuBar: Array<menu> = [
    {
      id: 1,
      name: 'Recipes',
      amount: 20,
    },
    {
      id: 2,
      name: 'Saved',
      amount: 75,
    },
    {
      id: 3,
      name: 'Following',
      amount: 248,
    },
  ];
  const [selected, setSelected] = useState(0);
  const [recipes, setRecipes] = useState(recipesData);
  // const [userProfile, setUserProfile] = useState(userProfileData);
  const [isLoading, setIsLoading] = useState(false);
  const userProfile = useSelector((state: RootStateOrAny) => state.auth.user);
  const dispatch = useDispatch();
  const recipesSave = useSelector(
    (state: RootStateOrAny) => state.recipes.recipesSaved,
  );

  const onSetSelectedHandle = (index: number) => {
    setSelected(index);
    if (index === 0) {
      setRecipes(recipesData);
    } else if (index === 1) {
      // RecipesStorage.retrieveRecipeData()
      //   .then(recipes => JSON.parse(recipes))
      //   .then(recipesJson => {
      //     setRecipes(recipesJson);
      //   });
      setRecipes(recipesSave);
    } else {
      setRecipes(recipesFollowingData);
    }
  };

  // const handleUpdateFromEdit = () => {
  //   UserProfileStorage.retrieveUserProfileData()
  //     .then(userProfile => JSON.parse(userProfile))
  //     .then(userProfileJson => setUserProfile(userProfileJson));
  // };

  const onLogoutHandle = () => {
    dispatch(authActions.logout(() => props.navigation.navigate(SCREEN.LOGIN)));
  };

  const RecipeList = () => {
    return (
      <FlatList
        keyExtractor={item => item.id}
        numColumns={2}
        data={recipes}
        renderItem={({item, index}) => <RecipeItem data={item} index={index} />}
        ItemSeparatorComponent={() => <View style={{height: 10}} />}
      />
    );
  };

  const Avatar = () => {
    return (
      <Image
        source={{uri: userProfile.avatar}}
        style={{
          width: Scale.scale(80),
          height: Scale.scale(80),
          borderRadius: Scale.scale(40),
        }}
      />
    );
  };

  const RecipeListWithLoading = WithLoading(RecipeList);
  const AvatarWithHoverOpacity = WithHoverOpacity(Avatar);

  useEffect(() => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
  }, []);

  return (
    <View style={{flex: 1, padding: 10}}>
      <View style={styles.header}>
        <Text
          style={{
            fontSize: Scale.scale(25),
            fontWeight: 'bold',
          }}>
          My Kitchen
        </Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <Image style={{marginRight: 5}} source={Images.img_settings} />
          </TouchableOpacity>
          <Text style={{fontWeight: 'bold', color: Colors.primary}}>
            Settings
          </Text>
        </View>
      </View>
      <View
        style={{
          flex: 0.38,
          justifyContent: 'space-between',
        }}>
        <View />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row'}}>
            <AvatarWithHoverOpacity />
            <View style={{marginLeft: 10}}>
              <Text style={{marginTop: Scale.scale(5), fontWeight: 'bold'}}>
                {userProfile.fullName}
              </Text>
              <Text>{userProfile.bio}</Text>
              <View style={styles.lastRow}>
                <Text style={{fontSize: 12}}>584 followers</Text>
                <Text>·</Text>
                <Text style={{fontSize: 12}}>23k likes</Text>
              </View>
            </View>
          </View>
          <View style={styles.rightTop}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() =>
                props.navigation.navigate(SCREEN.PROFILE_EDITING, {
                  // onSelect: handleUpdateFromEdit,
                  userProfile: userProfile,
                })
              }>
              <Image source={Images.img_edit} />
            </TouchableOpacity>
            <TouchableOpacity onPress={onLogoutHandle}>
              <Text style={styles.logoutText}>Log out</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: 1, backgroundColor: '#ccc'}} />
        <View style={styles.menuWrapper}>
          {menuBar.map((menu, index) => (
            <TouchableOpacity
              style={{width: 90}}
              key={menu.id}
              onPress={() => onSetSelectedHandle(index)}
              activeOpacity={0.8}>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={[
                    styles.amountText,
                    selected === index && {color: 'black'},
                  ]}>
                  {menu.amount}
                </Text>
                <Text
                  style={[
                    {fontSize: 15, color: '#ccc'},
                    selected === index && {color: 'black'},
                  ]}>
                  {menu.name}
                </Text>
              </View>
              {selected === index && <View style={styles.lineBottomMenu} />}
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <View style={{flex: 0.55, backgroundColor: '#F2F2F2'}}>
        <RecipeListWithLoading isLoading={isLoading} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  amountText: {
    color: '#ccc',
    marginBottom: 15,
    fontSize: 20,
    fontWeight: 'bold',
  },
  header: {
    flex: 0.07,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  lastRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 140,
    marginTop: Scale.scale(15),
  },
  menuWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: Scale.scale(15),
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  lineBottomMenu: {
    height: 3,
    width: 90,
    backgroundColor: Colors.primary,
    marginTop: 5,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  logoutText: {
    color: Colors.primary,
    fontWeight: 'bold',
  },
  rightTop: {
    flexDirection: 'row',
  },
});
