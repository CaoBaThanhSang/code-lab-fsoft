import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Gallery} from '../../components';

export const RecipeEditScreen = () => {
  const [isVisible, setIsVisible] = useState(false);
  const onCloseHandle = () => {
    setIsVisible(false);
  };
  const onOpenModalHandle = () => {
    setIsVisible(true);
  };
  return (
    <View style={styles.container}>
      <Gallery
        isVisible={isVisible}
        onClose={onCloseHandle}
        onOpenModal={onOpenModalHandle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
});
