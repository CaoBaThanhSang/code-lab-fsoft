import AsyncStorage from '@react-native-community/async-storage';

export const storeUserProfileData = async (userProfile: string) => {
  try {
    await AsyncStorage.setItem('@userProfile', userProfile);
  } catch (error) {
    console.log(error);
  }
};

export const retrieveUserProfileData = async () => {
  try {
    const userProfile = await AsyncStorage.getItem('@userProfile');
    if (userProfile !== null) {
      return userProfile;
    }
    return '';
  } catch (error) {
    console.log(error);
  }
};
